
*********************************************************
*********************************************************

Norwegian translation of the Drupal interface.  (Status 100%)

Please report all bugs, suggested changes and improvements at:

http://www.drupalnorge.no

As of time of writing, the website "Drupal Norge"  which is to be found
at http://drupal.norgesportal.no is under development and is expected
to be launched 28 February 2007(can be earlier so check the site).

NOTE TO NORWEGIAN USERS: Help us. If you find any mistake in grammar,
typographical errors and such, we hope that you don't just correct
your site. Open the no.po file on a text editor that displays line numbers.
Note the line number/s and your correction/s. Submit an issue at:
http://www.drupalnorge.no/innlegg/prosjekt/oversettelse/drupal-kjerne/no-po
Noting all your changes will be easier for you to install a newer version
of the translation, as well.

*********************************************************
*********************************************************

Modules are also being added as we progress with the translation effort.

*********************************************************
*********************************************************

Drupal Norge

On the translation development's official site, Drupal Norge, you can
track cases regarding translation files per module for the Drupal core
and contributed modules. You can also help in the decision-making
process of standardizing the translation of module names and
commonly used Drupal terminologies.

If you want to get involved in this project and/or other norwegian-
based drupal projects, please visit the Drupal Norge website. Though
development happens there, all official releases for this project will be
available at drupal.org


Instructions

To configure your Drupal site using the 'no' iso-639 language code,
go to /admin/settings/locale/language/add then choose Norwegian
(Norsk) from the drop downlist.This is the translation file/s if you want
to use iso-639 language code no which Drupal defaults to.

Alternatively, depending on the file you downloaded....
	* You may use the Norwegian (Nynorsk) translation and manually enter
	nno in the Language code on the Custom language field group.
    * You may use the Norwegian (Bokmål) translation and manually enter
	nb in the Language code on the Custom language field group.

*********************************************************
*********************************************************

Acknowledgments:

We would like to thank:

Drupal Norge
Drupal Danmark
John Noceda
Morten Wulff
Odd-Inge Erland

